# reform-hw-setup dracut module

This is a dracut module for the [MNT Reform 2](https://mntre.com/reform.html) laptop. It runs the [reform-hw-setup](https://source.mnt.re/reform/reform-tools/-/blob/main/sbin/reform-hw-setup) during bootup.

A copy of the script is included in this repository, but you can replace it
with an upstream copy without issue.

## Compatibility

As of now, the module supports the following models of the MNT Reform 2:

* MNT Reform 2 with the stock i.MX8M
* MNT Reform 2 with BPI-CM4 Module
* MNT Reform 2 with LS1028A Module

## Dependencies

For the module to work, you will need to have the following utilities installed on your system:

* amixer (stock, BPI-CM4)
* gpioset 1.X (LS1028A)

## Installation

Create a directory for the module in `/usr/lib/dracut/modules.d/`, then copy the shell scripts from this repository into this directory. Make sure the files are owned by root and are executable:
```bash
mkdir /usr/lib/dracut/modules.d/99reform-hw-setup

cp *.sh /usr/lib/dracut/modules.d/99reform-hw-setup/

chown root:root /usr/lib/dracut/modules.d/99reform-hw-setup/*.sh
chmod 0755 /usr/lib/dracut/modules.d/99reform-hw-setup/*.sh
```

## How it works

The module hooks a tiny script into pre-trigger. All this script does is to add the reform-hw-setup script to initqueue.

reform-hw-setup fixes issues with the keyboard on LS1028A. Because of that, it's important it's run before the cryptsetup password prompt. Since scripts in initqueue are run in alphabetical order, I added the 001 prefix to the reform-hw-setup script, so it's run before the cryptsetup ask password script.
